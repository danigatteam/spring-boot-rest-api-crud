package com.quintopino.springbootexample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quintopino.springbootexample.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}
